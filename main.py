from data_load import points_and_distance_matrix_from_points_file
from plotting import *
from evaluation import *
from prim import *
import local_search
from local import GreedyLocalSearch, SteepestLocalSearch

import numpy as np


def total_cost_from_clusters_list(clusters: List[InitialHeuristicCluster]):
    cost = 0
    for cluster in clusters:
        cost += cluster.cost
    return cost


# runs local search on clusters that were created by previously running initial heuristic, returns cost after run
def improve_with_local_search_after_initial_heuristic(distances, local_search_clusters):
    assert LOCAL_SEARCH_MODE == "GREEDY" or LOCAL_SEARCH_MODE == "STEEPEST"
    ls = {
        'GREEDY': GreedyLocalSearch,
        'STEEPEST': SteepestLocalSearch
    }[LOCAL_SEARCH_MODE](distances, local_search_clusters, candidates=CANDIDATES, use_cache=USE_CACHE)

    print("Before local search start: ", ls.cost)
    ls.run()
    print("After local search end: ", str(ls.cost))
    return ls.cost

def run_experiment():
    points, distance_matrix = points_and_distance_matrix_from_points_file("data/objects20_06.data")
    global global_distance_matrix
    global_distance_matrix = distance_matrix
    experiment: ExperimentRun = ExperimentRun()
    experiment.runs_no = RUNS_NO
    local_search.global_distance_matrix = distance_matrix
    try:
        for x in range(RUNS_NO):
            print(f"attempt #{x}")
            with experiment.measure_time():
                if INITIAL_SOLUTION == 'HEURISTIC':
                    local_search_clusters = prims_with_heuristic(
                        distance_matrix,
                        find_best_possible_append_greedy,
                        get_cost_for_distance
                    )
                else:
                    local_search_clusters = random_solution(CLUSTERS_NO, len(distance_matrix))

                cost_after_run = improve_with_local_search_after_initial_heuristic(distance_matrix, local_search_clusters)
            experiment.add_value(cost_after_run)

            experiment.conditionally_change_best_result(local_search_clusters, cost_after_run)
            #verify_all_vertices_used_and_only_once(local_search_clusters, len(points))
            if experiment.is_better(cost_after_run):
                draw_cluster_points_with_various_colours(local_search_clusters, points)
    except KeyboardInterrupt:
        print("interrupted")
    experiment.print_experiment_results()
    # draw_points_and_connections(experiment.best_result, points)

run_experiment()


# points, distance_matrix = points_and_distance_matrix_from_points_file("data/objects20_06.data")
# for x in neighbourhood_generator([
#     LocalSearchCluster(None, distance_matrix, vertices = set([1,2,3,4])),
#                        LocalSearchCluster( None, distance_matrix, vertices = set([7,8,9])),
#                         LocalSearchCluster( None, distance_matrix, vertices = set([10, 11, 12]))]):
#     print(x.from_cluster)
#     print(x.to_cluster)
#     print(x.vertex)
#     print("---")


