#!/bin/bash

experiment() {
    python3 main.py STEEPEST HEURISTIC $1 $2
    git add results.txt
    git commit -m "add results with candidates:$1, cache:$2"
    git push
}

experiment 0 False
experiment 0 True
experiment 25 False
experiment 25 True
