import random
from model import *
from typing import *


def distance_from_connections(distance_matrix, connections):
    distance_sum = 0
    for connection in connections:
        print(connection)
        distance_sum += distance_matrix[connection[0]][connection[1]]
    return distance_sum


def get_cost_for_distance(distance_matrix, vert1_vert2_tuple, _):
    return distance_matrix[vert1_vert2_tuple[0]][vert1_vert2_tuple[1]]


def get_cost_for_avg_dist_in_group(distance_matrix, new_vertex_old_vertex_tuple, cluster: InitialHeuristicCluster):
    cost_to_add = 0
    for vertex in cluster.vertices:
        cost_to_add += distance_matrix[vertex][new_vertex_old_vertex_tuple[0]]
    return cost_to_add + cluster.cost


def find_best_possible_append_greedy(solution_clusters: List[InitialHeuristicCluster]):
    minimal_append_cost = BIG_NUMBER
    best_possible_append: PossibleAppend = None

    for cluster in solution_clusters:
        best_cluster_append = cluster.find_best_possible_append_in_cluster()
        if best_cluster_append.cost < minimal_append_cost:
            minimal_append_cost = best_cluster_append.cost
            best_possible_append = best_cluster_append
    return best_possible_append


def find_best_possible_append_regret(solution_clusters: List[InitialHeuristicCluster]):
    biggest_regret = None
    for cluster in solution_clusters:
        cluster_regret = cluster.find_regret_for_cluster(3)
        if biggest_regret is None or cluster_regret[0] < biggest_regret[0]:
            biggest_regret = cluster_regret
    return biggest_regret[1].find_best_possible_append_in_cluster()


# # # # Main algorithm # # # #
def prims_with_heuristic(distance_matrix, find_best_possible_append_fn, get_append_cost_fn):
    vertices_no = len(distance_matrix)

    visited_vertices = set()
    solution_clusters: List[InitialHeuristicCluster] = [InitialHeuristicCluster() for _ in range(CLUSTERS_NO)]

    # clusters initial vertex choosing
    unvisited_vertices_temp = [i for i in range(len(distance_matrix))]
    for cluster_no in range(CLUSTERS_NO):
        initial_vertex = random.choice(unvisited_vertices_temp)
        solution_clusters[cluster_no].vertices.append(initial_vertex)
        solution_clusters[cluster_no].initial_vertex = initial_vertex
        visited_vertices.add(initial_vertex)
        unvisited_vertices_temp.remove(initial_vertex)

    # initialize appends in cluster with initial vertices
    for cluster in solution_clusters:
        for i in range(0, vertices_no):
            if i not in visited_vertices:
                cluster.possible_appends.append(
                    PossibleAppend(i, get_append_cost_fn(distance_matrix, (i, cluster.initial_vertex), cluster), cluster.initial_vertex, cluster))

    while True:
        if len(visited_vertices) == vertices_no:
            break
        select_and_handle_move(distance_matrix, find_best_possible_append_fn, solution_clusters, visited_vertices, get_append_cost_fn)
    return [set(cluster.vertices) for cluster in solution_clusters]


def select_and_handle_move(distance_matrix, find_best_possible_append_fn, solution_clusters, visited_vertices, get_append_cost_fn):
    best_possible_append = find_best_possible_append_fn(solution_clusters)
    best_possible_append.cluster.vertices.append(best_possible_append.which_ind)
    best_possible_append.cluster.selected_edges.append(
        Edge(best_possible_append.which_ind, best_possible_append.by_which_ind))
    best_possible_append.cluster.cost += best_possible_append.cost
    visited_vertices.add(best_possible_append.which_ind)
    index_of_vertex_to_remove_from_possible_appends = best_possible_append.which_ind
    for cl in solution_clusters:
        # removing append of vertex which was already added
        for app_rem in cl.possible_appends:
            if app_rem.which_ind == index_of_vertex_to_remove_from_possible_appends:
                cl.possible_appends.remove(app_rem)
    for append in best_possible_append.cluster.possible_appends:
        new_cost = get_append_cost_fn(distance_matrix,(best_possible_append.which_ind, append.which_ind), best_possible_append.cluster)
        if append.cost > new_cost:
            append.cost = new_cost
            append.by_which_ind = best_possible_append.which_ind


