import matplotlib.pyplot as plt
import random
from local_search import *


def random_hex_color():
    return "#%06x" % random.randint(0, 0xFFFFFF)


cluster_colors = [
    "#ABCDEF",
    "#DAAD86",
    "#8EE4AF",
    "#FC4445",
    "#378683",
    "#5D5C61",
    "#3500D3",
    "#AFD275",
    "#EFAFBC",
    "#EEE2DC",
    "#e6c6ca",
    "#78232e",
    "#4b161d",
    "#84725a",
    "#f7e5cd",
    "#a4b090",
    "#e1e8d3",
    "#8a8492",
    "#534d59",
    "#60992d",
    "#e1e8d3"
]


def draw_points_and_connections(clusters: List[InitialHeuristicCluster], points):
    zipped = list(zip(*points))
    plt.scatter(zipped[0], zipped[1])

    for cluster in clusters:
        cluster_color = cluster_colors[clusters.index(cluster)]
        connections_coordinates = []
        for x in cluster.selected_edges:
            connections_coordinates.append((points[x.from_ind], points[x.to_ind]))
        for conn in connections_coordinates:
            # plotting line between two points
            plt.plot([conn[0][0], conn[1][0]], [conn[0][1], conn[1][1]],
                     linewidth=1, color=cluster_color)
    plt.show()
    # plt.savefig("viz.svg")


def draw_cluster_points_with_various_colours(clusters: List[LocalSearchCluster], points):
    zipped = list(zip(*points))
    # plt.scatter(zipped[0], zipped[1])

    for cluster in clusters:
        cluster_color = cluster_colors[clusters.index(cluster)]
        cluster_xs = []
        cluster_ys = []
        for vertex_no in cluster:
            cluster_xs.append(points[vertex_no][0])
            cluster_ys.append(points[vertex_no][1])
        plt.scatter(cluster_xs, cluster_ys, color=cluster_color)
    plt.savefig(f"{INITIAL_SOLUTION}_{LOCAL_SEARCH_MODE}.png")
