import numpy as np
import math

#   input file path in string format
#   returns list of points [(x1,y1), (x2,y2) ... ]
def load_points_from_file(file_path: str):
    file = open(file_path)
    points_str = list(map(lambda x: x.strip().split(), file.readlines()))
    points_int = list(map(lambda x: (int(x[0]), int(x[1])), points_str))
    return points_int

#   input list of points [(x1,y1), (x2,y2) ... ]
#   returns matrix of distances between points
def points_to_distance_matrix(points):
    n = len(points)
    distances = np.empty((n, n))
    for i, (x1, y1) in enumerate(points):
        for j, (x2, y2) in enumerate(points):
            distances[i, j] = math.hypot(x1 - x2, y1 - y2)
    return distances


def points_and_distance_matrix_from_points_file(file_path: str):
    points = load_points_from_file(file_path)
    return points, points_to_distance_matrix(points)
