import itertools
import random

from model import *

global_distance_matrix = None


def get_all_possible_edges_distance(vertices: Set[int], distance_matrix):
    distance_sum = 0.0
    vertices_list = list(vertices)
    for i in range(len(vertices)):
        for j in range(i + 1, len(vertices_list)):
            distance_sum += distance_matrix[vertices_list[i], vertices_list[j]]
    return distance_sum


def how_many_possible_edges(vertices):
    return int((len(vertices) * (len(vertices) - 1)) / 2)


# represents cluster in local search - smaller scope of fields needed
class LocalSearchCluster:
    vertices: Set = None
    all_distances_sum: int = None

    # equivalent to how many edges are in a clique
    def how_many_possible_edges(self):
        return how_many_possible_edges(self.vertices)

    def recount_possible_edges_distance_sum(self, distance_matrix):
        self.all_distances_sum = get_all_possible_edges_distance(self.vertices, distance_matrix)

    def __init__(self, initial_heuristic_cluster: InitialHeuristicCluster, distance_matrix, vertices = None):
        if vertices is None:
            self.vertices = set(initial_heuristic_cluster.vertices)
            self.recount_possible_edges_distance_sum(distance_matrix)
        else:
            self.vertices = vertices
            self.recount_possible_edges_distance_sum(distance_matrix)


class VertexChange:
    def __init__(self, from_cluster, to_cluster, vertex):
        self.from_cluster = from_cluster
        self.to_cluster = to_cluster
        self.vertex = vertex

    from_cluster: int = None
    to_cluster: int = None
    vertex: int = None


def random_solution(k, n):
    vertices_sets = [set() for _ in range(k)]
    for i in range(n):
        vertices_sets[random.randrange(k)].add(i)
    return vertices_sets

# sasiedztwo -> przelaczenia jednego wierzcholka z jednego klastra do drugiego
def neighbourhood_generator(clusters: List[LocalSearchCluster]):
    for cluster1 in clusters:
        for cluster2 in clusters:
            if cluster1 is not cluster2:
                for vertex in cluster1.vertices:
                    if len(cluster1.vertices) > 1:
                        yield VertexChange(cluster1, cluster2, vertex)

def shuffle_iterator(iterator, chunk_size=50):
    while True:
        chunk = list(itertools.islice(iterator, chunk_size))
        if not chunk:
            break
        random.shuffle(chunk)
        yield from chunk

def randomized_neighbourhood_generator(clusters):
    return shuffle_iterator(neighbourhood_generator(clusters))

# assumes clusters have up to date distances sum in them uncless non_cached_distances is set to True
def cost_for_local_from_clusters(clusters: List[LocalSearchCluster], non_cached_distances=False, with_total=False):
    possible_edges_distance_sum = 0.0
    possible_edges = 0
    for cluster in clusters:
        if non_cached_distances:
            cluster.recount_possible_edges_distance_sum(global_distance_matrix)
        possible_edges_distance_sum += cluster.all_distances_sum
        possible_edges += cluster.how_many_possible_edges()
    avg_distance = possible_edges_distance_sum / possible_edges
    if with_total:
        return avg_distance, possible_edges
    else:
        return avg_distance

# assumes clusters have up to date distances sum in them
def evaluate_delta(clusters: List[LocalSearchCluster], vertex_change: VertexChange, cost_before=None, total_before=None):
    from_cluster = vertex_change.from_cluster
    to_cluster = vertex_change.to_cluster
    vertex = vertex_change.vertex

    distances_from_cluster = sum(global_distance_matrix[i, vertex] for i in from_cluster.vertices if i != vertex)
    distances_to_cluster = sum(global_distance_matrix[i, vertex] for i in to_cluster.vertices)

    difference_of_sum = distances_to_cluster - distances_from_cluster

    if cost_before is None or total_before is None:  
        cost_before, total_before = cost_for_local_from_clusters(clusters, with_total=True)
    sum_before = cost_before * total_before
    total_after = total_before + len(to_cluster.vertices) - len(from_cluster.vertices) + 1

    cost_after = (sum_before + difference_of_sum) / total_after

    # delta < 0 -> zysk, delta > 0 -> strata
    return cost_after - cost_before



def execute_change(clusters: List[LocalSearchCluster], vertex_change: VertexChange):
    vertex_change.from_cluster.vertices.remove(vertex_change.vertex)
    vertex_change.to_cluster.recount_possible_edges_distance_sum(global_distance_matrix)
    vertex_change.to_cluster.vertices.add(vertex_change.vertex)
    vertex_change.from_cluster.recount_possible_edges_distance_sum(global_distance_matrix)
    
def change_one_greedy_local(clusters: List[LocalSearchCluster]):
    cost_before, total_before = cost_for_local_from_clusters(clusters, with_total=True)
    for vertex_change in randomized_neighbourhood_generator(clusters):
        if evaluate_delta(clusters, vertex_change, cost_before, total_before) < 0:
            execute_change(clusters, vertex_change)
            return


def change_one_steepest_local(clusters: List[LocalSearchCluster]):
    best_delta = BIG_NUMBER
    best_change: VertexChange = None
    cost_before, total_before = cost_for_local_from_clusters(clusters, with_total=True)
    for vertex_change in randomized_neighbourhood_generator(clusters):
        delta_for_change = evaluate_delta(clusters, vertex_change, cost_before, total_before)
        if delta_for_change < best_delta:
            best_change = vertex_change
            best_delta = delta_for_change
    if best_delta < 0:
        execute_change(clusters, best_change)
