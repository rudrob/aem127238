from model import *
from local_search import LocalSearchCluster


def get_cost_from_clusters(clusters: List[InitialHeuristicCluster]):
    total_cost = 0
    for cluster in clusters:
        total_cost += cluster.cost
    return total_cost


def verify_solution(clusters: List[InitialHeuristicCluster], distance_matrix):
    vertices = set()
    for cluster in clusters:
        for vertex in cluster.vertices:
            assert vertex not in vertices
            vertices.add(vertex)
        cluster_cost = 0
        for edge in cluster.selected_edges:
            cluster_cost += distance_matrix[edge.from_ind, edge.to_ind]
        assert(cluster_cost == cluster.cost)


def verify_all_vertices_used_and_only_once(clusters: List[LocalSearchCluster], vertices_no):
    vertices = set()
    for cluster in clusters:
        for vertex in cluster.vertices:
            assert vertex not in vertices
            vertices.add(vertex)
    for x in range(vertices_no):
        assert(x in vertices)




