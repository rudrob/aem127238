from local_base import ListBaseLocalSearch as BaseLocalSearch

class GreedyLocalSearch(BaseLocalSearch):
    def run(self):
        while True:
            change_and_cost = next((
                neighbour for neighbour
                in self.neighbourhood_with_costs(self.solution, randomize=True)
                if neighbour[1][0] / neighbour[1][1] < self.cost
            ), None)

            if change_and_cost is None:
                break
            self.apply_change(change_and_cost[0], change_and_cost[1])
        return self.solution

class SteepestLocalSearch(BaseLocalSearch):
    def run(self):
        change_cost = lambda x: x[1][0] / x[1][1]
        while True:
            change, cost_after_change = min(self.neighbourhood_with_costs(self.solution), key=change_cost)
            if cost_after_change[0] / cost_after_change[1] >= self.cost:
                break
            self.apply_change(change, cost_after_change)
        return self.solution
