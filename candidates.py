import numpy as np

def calculate_nearby_top_k(distances, k):
    n = distances.shape[0]
    return [set(np.argpartition(distances[i, :], k+1)[1:k+1]) for i in range(n)]
