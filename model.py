from typing import *
from statistics import mean, stdev
import time
import contextlib
import sys

CLUSTERS_NO = 20
BIG_NUMBER = 1000000000
RUNS_NO = 100

# greedy or steepest
LOCAL_SEARCH_MODE = sys.argv[1]

# heuristic or random
INITIAL_SOLUTION = sys.argv[2]

# enhancements
CANDIDATES = int(sys.argv[3])
if CANDIDATES == 0:
    CANDIDATES = None

USE_CACHE = sys.argv[4] == 'True'

global_distance_matrix = None


class ExperimentRun:
    runs_no = None
    min_value = None
    best_result = None

    def __init__(self):
        self.values = []
        self.durations = []

    def add_value(self, value):
        self.values.append(value)
        if self.min_value is None or value < self.min_value:
            self.min_value = value
    
    @contextlib.contextmanager
    def measure_time(self):
        t0 = time.time()
        yield
        t1 = time.time()
        self.durations.append(t1 - t0)

    def get_stdev_value(self):
        return stdev(self.values)

    def print_experiment_results(self):
        values = (min(self.values), mean(self.values), max(self.values))
        durations = (min(self.durations), mean(self.durations), max(self.durations))

        print("Runs " + str(self.runs_no))

        print("Values min/avg/max:", values)
        print("Stdev of values", str(self.get_stdev_value()))
        print("Time min/avg/max:", durations)

        with open('results.txt', 'at') as f:
            f.write(str({
                'initial': INITIAL_SOLUTION,
                'local_search': LOCAL_SEARCH_MODE,
                'candidates': CANDIDATES,
                'cache': USE_CACHE,
                'values': values,
                'durations': durations,
                'best_result': self.best_result,
            }) + "\n")

    def is_better(self, cost):
        return self.best_result is None or self.min_value is None or cost <= self.min_value

    def conditionally_change_best_result(self, result, cost):
        if self.is_better(cost):
            self.best_result = result

class Edge:
    from_ind: int
    to_ind: int

    def __init__(self, from_ind, to_ind):
        if from_ind <= to_ind:
            self.from_ind = from_ind
            self.to_ind = to_ind
        else:
            self.to_ind = from_ind
            self.from_ind = to_ind


class PossibleAppend:
    which_ind: int
    cost: float
    by_which_ind: int
    cluster = None

    def __init__(self, which_ind, cost, by_which_ind, cluster):
        self.which_ind = which_ind
        self.cost = cost
        self.by_which_ind = by_which_ind
        self.cluster = cluster


# represents cluster in initial heuristic building - wide field scope, with possible appends etc.
class InitialHeuristicCluster:

    def __init__(self):
        self.cost = 0
        self.vertices = []
        self.selected_edges = []
        self.initial_vertex = -1
        self.possible_appends = []

    cost: float
    vertices: List[int] = None
    selected_edges: List[Edge]
    initial_vertex: int
    possible_appends: List[PossibleAppend]

    def find_best_possible_append_in_cluster(self):
        best_possible_append = None
        best_cost = BIG_NUMBER
        for append in self.possible_appends:
            if append.cost < best_cost:
                best_possible_append = append
                best_cost = append.cost
        return best_possible_append

    def find_regret_for_cluster(self, k):
        self.possible_appends.sort(key=lambda app: app.cost)
        regret_sum = 0
        for i in range(1, k + 1):
            if len(self.possible_appends) > k:
                regret_sum += self.possible_appends[i].cost - self.possible_appends[0].cost
        return regret_sum, self
