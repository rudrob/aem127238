import random
import numpy as np

from candidates import calculate_nearby_top_k

class ListBaseLocalSearch:
    def __init__(self, distances, solution_list, candidates=None, use_cache=False):
        self.distances = distances
        self.set_solution(solution_list)
        self.last_change = None

        self.use_candidates = False
        self.use_cache = False
        if candidates is not None:
            self.setup_candidates(candidates)
        if use_cache:
            self.setup_cache()

    def setup_candidates(self, top):
        self.use_candidates = True
        self.nearby_vertices = calculate_nearby_top_k(self.distances, top)
        self.where_is = np.zeros(self.n, dtype=int)
        for c, cluster in enumerate(self.solution):
            for v in cluster:
                self.where_is[v] = c
    
    def setup_cache(self):
        self.use_cache = True
        self.cache = {}
        self.evaluate_change = self.evaluate_change_with_cache
    
    def set_solution(self, solution):
        self.solution = solution
        self.total_length, self.total_edges = self.evaluate(solution)
        self.cost = self.total_length / self.total_edges
        self.k = len(self.solution)
        self.n = max(max(cluster) for cluster in solution) + 1
    
    def evaluate(self, solution):
        total_length = 0.0
        total_edges = 0
        for cluster in solution:
            size = len(cluster)
            total_length += sum(self.distances[v1, v2] for v2 in cluster for v1 in cluster if v1 < v2)
            total_edges += size * (size - 1) / 2
        return total_length, total_edges
    
    def run(self):
        raise NotImplementedError

    def _random(self, _):
        return random.random()
    
    def neighbourhood(self, solution, randomize=False):
        clusters = range(self.k)
        if randomize:
            clusters = list(clusters)
            clusters.sort(key=self._random)
        
        for source in clusters:
            if len(solution[source]) <= 1:
                continue
            for vertex in solution[source]:
                if self.use_candidates:
                    destinations = set(self.where_is[j] for j in self.nearby_vertices[vertex])
                else:
                    destinations = sorted(clusters, key=self._random)
                
                for destination in destinations:
                    if source != destination:
                        yield (source, vertex, destination)

    def neighbourhood_with_costs(self, solution, randomize=False):
        for change in self.neighbourhood(solution, randomize):
            yield change, self.evaluate_change(change)

    def evaluate_change_with_cache(self, change):
        if change in self.cache:
            delta_length, delta_edges = self.cache[change]
        else:
            delta_length, delta_edges = self.evaluate_change_delta(change)
            self.cache[change] = (delta_length, delta_edges)
        total_length = self.total_length + delta_length
        total_edges = self.total_edges + delta_edges
        return total_length, total_edges
    
    def evaluate_change(self, change):
        source, vertex, destination = change
        new_total_length = self.total_length
        new_total_edges = self.total_edges + 1 - len(self.solution[source]) + len(self.solution[destination])
        for i in self.solution[source]:
            new_total_length -= self.distances[vertex, i]
        for j in self.solution[destination]:
            new_total_length += self.distances[vertex, j]
        return new_total_length, new_total_edges
    
    def evaluate_change_delta(self, change):
        source, vertex, destination = change
        delta_length = 0.0
        for i in self.solution[source]:
            delta_length -= self.distances[vertex, i]
        for j in self.solution[destination]:
            delta_length += self.distances[vertex, j]
        delta_edges = 1 - len(self.solution[source]) + len(self.solution[destination])
        return delta_length, delta_edges
    
    def apply_change(self, change, cost=None):
        if cost is None:
            cost = self.evaluate_change(change)
        self.last_change = change
        source, vertex, destination = change
        self.solution[source].remove(vertex)
        self.solution[destination].add(vertex)
        self.total_length, self.total_edges = cost
        self.cost = self.total_length / self.total_edges
        if self.use_candidates:
            self.where_is[vertex] = destination
        if self.use_cache:
            self.invalidate_cache(source, destination)
    
    def invalidate_cache(self, source, destination):
        keys_to_delete = list(
            key for key in self.cache.keys()
            if key[0] == source or key[2] == source or key[0] == destination or key[2] == destination
        )
        for key in keys_to_delete:
            del self.cache[key]
